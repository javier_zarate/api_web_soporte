package ttib.websoporte.ttibapiwebsoporte.services;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ttib.websoporte.ttibapiwebsoporte.models.BankParameter;
import ttib.websoporte.ttibapiwebsoporte.repositories.BankParameterRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("bankParameterService")
public class BankParameterService {
	private BankParameterRepository repository;
	public BankParameterService(BankParameterRepository repository) {
		this.repository = repository;
	}
	public List<BankParameter> getAllBanks()
    {
        List<BankParameter> result = (List<BankParameter>) repository.findAll();
         
        if(result.size() > 0) {
            return result;
        } else {
            return new ArrayList<BankParameter>();
        }
    }
	public void getBankById(String id,String nombre,String entidad, String estado) throws Exception
    {
        Optional<BankParameter> bank = repository.findById(id);
        if(bank.isPresent()) {
        	BankParameter newEntity1 =  bank.get();
            newEntity1.setNombre(nombre);
            newEntity1.setEntidad(entidad);
            newEntity1.setEstado(estado);
            repository.save(newEntity1);
        } else {
            throw new Exception("No bank record exist for given id");
        }
    }
     
    public void createBank(String id,String nombre, String entidad, String estado)
    {
        BankParameter newEntity = new BankParameter();
        newEntity.setId(id);
        newEntity.setNombre(nombre);
        newEntity.setEntidad(entidad);
        newEntity.setEstado(estado);
        repository.save(newEntity);
                 
    } 
     
    public void deleteBankById(String id) throws Exception
    {
        Optional<BankParameter> bank = repository.findById(id);
         
        if(bank.isPresent()) 
        {
            repository.deleteById(id);
        } else {
            throw new Exception("No bank record exist for given id");
        }
    } 
}