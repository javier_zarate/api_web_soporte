package ttib.websoporte.ttibapiwebsoporte.services;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ttib.websoporte.ttibapiwebsoporte.models.ChannelParameter;
import ttib.websoporte.ttibapiwebsoporte.repositories.ChannelParameterRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("channelParameterService")
public class ChannelParameterService {
	private ChannelParameterRepository repository;
	public ChannelParameterService(ChannelParameterRepository repository) {
		this.repository = repository;
	}
	public List<ChannelParameter> getAllChannels() {
		List<ChannelParameter> result = (List<ChannelParameter>) repository.findAll();
        
        if(result.size() > 0) {
            return result;
        } else {
            return new ArrayList<ChannelParameter>();
        }
	}
	
	public void getChannelById(String id,String nombre,String entidad, String estado) throws Exception
    {
        Optional<ChannelParameter> channel = repository.findById(id);
        if(channel.isPresent()) {
        	ChannelParameter newEntity1 =  channel.get();
            newEntity1.setNombre(nombre);
            newEntity1.setEntidad(entidad);
            newEntity1.setEstado(estado);
            repository.save(newEntity1);
        } else {
            throw new Exception("No channel record exist for given id");
        }
    }
	
	public void createChannel(String id,String nombre, String entidad, String estado)
    {
        ChannelParameter newEntity = new ChannelParameter();
        newEntity.setId(id);
        newEntity.setNombre(nombre);
        newEntity.setEntidad(entidad);
        newEntity.setEstado(estado);
        repository.save(newEntity);
                 
    }
	
    public void deleteChannelById(String id) throws Exception
    {
        Optional<ChannelParameter> channel = repository.findById(id);
         
        if(channel.isPresent()) 
        {
            repository.deleteById(id);
        } else {
            throw new Exception("No coin record exist for given id");
        }
    }	
}