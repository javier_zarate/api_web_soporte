package ttib.websoporte.ttibapiwebsoporte.services;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ttib.websoporte.ttibapiwebsoporte.models.CoinParameter;
import ttib.websoporte.ttibapiwebsoporte.repositories.CoinParameterRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service("coinParameterService")
public class CoinParameterService {
	private CoinParameterRepository repository;
	public CoinParameterService(CoinParameterRepository repository) {
		this.repository = repository;
	}
	public List<CoinParameter> getAllCoins()
    {
        List<CoinParameter> result = (List<CoinParameter>) repository.findAll();
         
        if(result.size() > 0) {
            return result;
        } else {
            return new ArrayList<CoinParameter>();
        }
    }
     
    public void getCoinById(String id,String nombre,String simbolo, String estado) throws Exception
    {
        Optional<CoinParameter> coin = repository.findById(id);
        if(coin.isPresent()) {
        	CoinParameter newEntity1 =  coin.get();
        	newEntity1.setNombre(nombre);
        	newEntity1.setSimbolo(simbolo);
        	newEntity1.setEstado(estado);
        	repository.save(newEntity1);
        } else {
            throw new Exception("No coin record exist for given id");
        }
    }
     
    public void createCoin(String id,String nombre, String simbolo, String estado)
    {
    	CoinParameter newEntity = new CoinParameter();
    	newEntity.setId(id);
    	newEntity.setNombre(nombre);
        newEntity.setSimbolo(simbolo);
        newEntity.setEstado(estado);
    	repository.save(newEntity);
                 
    } 
     
    public void deleteCoinById(String id) throws Exception
    {
        Optional<CoinParameter> coin = repository.findById(id);
         
        if(coin.isPresent()) 
        {
            repository.deleteById(id);
        } else {
            throw new Exception("No coin record exist for given id");
        }
    } 
}