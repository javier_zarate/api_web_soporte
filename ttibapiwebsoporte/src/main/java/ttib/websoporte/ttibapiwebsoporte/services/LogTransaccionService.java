package ttib.websoporte.ttibapiwebsoporte.services;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ttib.websoporte.ttibapiwebsoporte.models.LogTransaccion;
import ttib.websoporte.ttibapiwebsoporte.repositories.LogTransaccionRepository;
import java.util.ArrayList;
import java.util.List;
import java.text.*;


@Service("logTransaccionService")
public class LogTransaccionService {

	private LogTransaccionRepository repository;

	public LogTransaccionService(LogTransaccionRepository repository) {
		this.repository = repository;
	}

	public List<LogTransaccion> getResultsLog(String fecha1, String hora1, Integer pageNo, Integer pageSize, String sortBy) throws Exception {
		String fechacompleta = fecha1 + " "+ hora1;
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<LogTransaccion> pagedResult = repository.findByFechaTransaccion(
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechacompleta), 
				paging);
		
		if (pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<>();
		}
		
	}
}