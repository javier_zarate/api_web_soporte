package ttib.websoporte.ttibapiwebsoporte.models;

//import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//@Data
@Entity
@Table(name= "channelparameter")
public class ChannelParameter {
    @Id
    private @Getter @Setter String id;
	@Column
	private @Getter @Setter String Nombre;
	@Column
	private@ Getter @Setter  String Entidad;
	@Column
	private @Getter @Setter  String Estado;
}