package ttib.websoporte.ttibapiwebsoporte.models;

//import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


//@Data
@Entity
@Table(name= "logtransaccion")
public class LogTransaccion {
    @Id
    private @Getter @Setter  Integer id;
	@Column
	private @Getter @Setter  String bancoOrig;
	@Column
	private@ Getter @Setter  String bancoDest;
	@Temporal(TemporalType.TIME)
	private @Getter @Setter  Date horaTransaccion;
    @Temporal(TemporalType.DATE)
	private @Getter @Setter  Date fechaTransaccion;
	@Column
	private @Getter @Setter  String tipoTransferencia;
	@Column(name="proceso")
	private @Getter @Setter  String proceso;
	@Column(name="trace")
	private @Getter @Setter  String trace;
	@Column(name="canal")
	private @Getter @Setter  String canal;
	@Column(name="rol")
	private @Getter @Setter  String rol;
	@Column
	private @Getter @Setter  String codRes;
	@Column
	private @Getter @Setter  String codResAmp;
	@Column
	private @Getter @Setter  String codOrigRest;
	@Column
	private @Getter @Setter  String descripError;
	@Column
	private @Getter @Setter  String moneda;
	@Column
	private @Getter @Setter  String idTransCce;
	@Column
	private @Getter @Setter  String idTransCceOrig;
	@Column
	private @Getter @Setter  String idTransApp;
	@Column
	private @Getter @Setter  String fechaHoraAce;
	@Column
	private @Getter @Setter  String evento;
}