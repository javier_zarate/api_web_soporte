package ttib.websoporte.ttibapiwebsoporte.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ttib.websoporte.ttibapiwebsoporte.models.CoinParameter;

@Repository
public interface CoinParameterRepository extends CrudRepository<CoinParameter, String> {

}