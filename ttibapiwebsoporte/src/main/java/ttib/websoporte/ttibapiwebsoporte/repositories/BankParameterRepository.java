package ttib.websoporte.ttibapiwebsoporte.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ttib.websoporte.ttibapiwebsoporte.models.BankParameter;

@Repository
public interface BankParameterRepository extends CrudRepository<BankParameter, String> {

}

