package ttib.websoporte.ttibapiwebsoporte.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ttib.websoporte.ttibapiwebsoporte.models.LogTransaccion;
import java.util.Date;

import org.springframework.data.jpa.repository.Query;

@Repository
public interface LogTransaccionRepository extends PagingAndSortingRepository<LogTransaccion, Integer> {
	@Query(value="select * from LogTransaccion a where CONVERT(DATETIME,CONVERT(CHAR(8),a.fecha_transaccion, 112)+' '+CONVERT(CHAR(8),a.hora_transaccion,108))>=?1",nativeQuery=true)
    Page<LogTransaccion> findByFechaTransaccion(Date fecha1, Pageable pageable);

}