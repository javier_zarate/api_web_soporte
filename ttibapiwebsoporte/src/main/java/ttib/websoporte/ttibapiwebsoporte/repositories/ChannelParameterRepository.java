package ttib.websoporte.ttibapiwebsoporte.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ttib.websoporte.ttibapiwebsoporte.models.ChannelParameter;

@Repository
public interface ChannelParameterRepository extends CrudRepository<ChannelParameter, String> {

}
