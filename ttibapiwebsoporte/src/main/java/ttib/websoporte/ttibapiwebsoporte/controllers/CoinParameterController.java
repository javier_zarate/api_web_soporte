package ttib.websoporte.ttibapiwebsoporte.controllers;
import org.springframework.http.HttpHeaders;
import org.springframework.ui.Model;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ttib.websoporte.ttibapiwebsoporte.models.CoinParameter;
import ttib.websoporte.ttibapiwebsoporte.services.CoinParameterService;
import java.util.List;

@CrossOrigin(origins={"http://localhost:8081","https://wapceu2ttibd00.azurewebsites.net"})
@RestController

@RequestMapping("/api/coins")
public class CoinParameterController {
    private CoinParameterService service;
    public CoinParameterController(CoinParameterService service) {
        this.service = service;
    }
    @RequestMapping("/")
    public String indexcoins() {
        return "Monedas";
    }
    @GetMapping("/CoinParameters")
    public ResponseEntity<List<CoinParameter>> getAllCoins(Model model) 
    {
        List<CoinParameter> list = service.getAllCoins();
        model.addAttribute("coins", list);
        return new ResponseEntity<>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/CoinParameters/create")
    public ResponseEntity<CoinParameter> createCoin(
    		@RequestBody CoinParameter coinParameter
    ){
    	service.createCoin(
                coinParameter.getId(),
                coinParameter.getNombre(),
                coinParameter.getSimbolo(),
                coinParameter.getEstado()
                );
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }
    
    @PutMapping("/CoinParameters/edit/{id}")
    public ResponseEntity<CoinParameter> editCoinById(
    		@PathVariable String id,
    		@RequestBody CoinParameter coinParameter
    )
    		throws Exception
    {
        service.getCoinById(
        		id,
        		coinParameter.getNombre(),
        		coinParameter.getSimbolo(),
        		coinParameter.getEstado()
        		);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }
     
    @DeleteMapping("/CoinParameters/delete/{id}")
    public ResponseEntity<Void> deleteCoinById(
    		@PathVariable("id") String id
    )
            throws Exception 
    {
        service.deleteCoinById(id);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    } 
}  