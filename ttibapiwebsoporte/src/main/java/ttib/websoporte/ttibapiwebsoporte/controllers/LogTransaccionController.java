package ttib.websoporte.ttibapiwebsoporte.controllers;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ttib.websoporte.ttibapiwebsoporte.models.LogTransaccion;
import ttib.websoporte.ttibapiwebsoporte.services.LogTransaccionService;
import java.util.List;

@CrossOrigin(origins={"http://localhost:8081","https://wapceu2ttibd00.azurewebsites.net"})
@RestController

@RequestMapping("/api/operations")
public class LogTransaccionController {
    private LogTransaccionService service;

    public LogTransaccionController(LogTransaccionService service) {
        this.service = service;
    }

    @RequestMapping("/")
    public String index() {
        return "TTIB - Api - Web de Soporte - v1.0";
    }

    @RequestMapping("/default")
    public String index1() {
        return "Hola mundo";
    }

    @GetMapping("/LogTransacciones/")
    public ResponseEntity<List<LogTransaccion>> getAll(
    		@RequestParam String fecha1,
    		@RequestParam String hora1,
    		@RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy
    		)throws Exception {
        		List<LogTransaccion> list = service.getResultsLog(fecha1, hora1, pageNo, pageSize, sortBy);
        		return new ResponseEntity<>(list, new HttpHeaders(), HttpStatus.OK);
    }
}