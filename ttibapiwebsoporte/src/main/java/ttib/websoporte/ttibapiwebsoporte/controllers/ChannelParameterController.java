package ttib.websoporte.ttibapiwebsoporte.controllers;
import org.springframework.http.HttpHeaders;
import org.springframework.ui.Model;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ttib.websoporte.ttibapiwebsoporte.models.ChannelParameter;
import ttib.websoporte.ttibapiwebsoporte.services.ChannelParameterService;
import java.util.List;

@CrossOrigin(origins={"http://localhost:8081","https://wapceu2ttibd00.azurewebsites.net"})
@RestController
@RequestMapping("/api/channels")

public class ChannelParameterController {
    private ChannelParameterService service;
    public ChannelParameterController(ChannelParameterService service) {
        this.service = service;
    }
    @RequestMapping("/")
    public String indexchannels() {
        return "Canales";
    }
    @GetMapping("/ChannelParameters")
    public ResponseEntity<List<ChannelParameter>> getAllChannels(Model model) 
    {
        List<ChannelParameter> list = service.getAllChannels();
        model.addAttribute("channels", list);
        return new ResponseEntity<>(list, new HttpHeaders(), HttpStatus.OK);
    }
    
    @PostMapping("/ChannelParameters/create")
    public ResponseEntity<ChannelParameter> createChannel(
            @RequestBody ChannelParameter channelParameter
    ){
        service.createChannel(
        		channelParameter.getId(),
        		channelParameter.getNombre(),
        		channelParameter.getEntidad(),
        		channelParameter.getEstado()
                );
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }
    
    @PutMapping("/ChannelParameters/edit/{id}")
    public ResponseEntity<ChannelParameter> editChannelById(
            @PathVariable String id,
            @RequestBody ChannelParameter channelParameter
    )
            throws Exception
    {
        service.getChannelById(
                id,
                channelParameter.getNombre(),
                channelParameter.getEntidad(),
                channelParameter.getEstado()
                );
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }
     
    @DeleteMapping("/ChannelParameters/delete/{id}")
    public ResponseEntity<Void> deleteChannelById(
            @PathVariable("id") String id
    )
            throws Exception 
    {
        service.deleteChannelById(id);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    } 
}