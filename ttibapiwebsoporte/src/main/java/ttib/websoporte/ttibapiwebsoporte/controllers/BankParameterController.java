package ttib.websoporte.ttibapiwebsoporte.controllers;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ttib.websoporte.ttibapiwebsoporte.models.BankParameter;
import ttib.websoporte.ttibapiwebsoporte.services.BankParameterService;
import java.util.List;

@CrossOrigin(origins={"http://localhost:8081","https://wapceu2ttibd00.azurewebsites.net"})
@RestController
@RequestMapping("/api/banks")

public class BankParameterController {
    private BankParameterService service;
    public BankParameterController(BankParameterService service) {
        this.service = service;
    }
    @RequestMapping("/")
    public String indexbanks() {
        return "Bancos";
    }
    @GetMapping("/BankParameters")
    public ResponseEntity<List<BankParameter>> getAllBanks(Model model) 
    {
        List<BankParameter> list = service.getAllBanks();
        model.addAttribute("banks", list);
        return new ResponseEntity<>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/BankParameters/create")
    public ResponseEntity<BankParameter> createBank(
    		@RequestBody BankParameter bankParameter
    ){
    	service.createBank(
    			bankParameter.getId(),
    			bankParameter.getNombre(),
    			bankParameter.getEntidad(),
    			bankParameter.getEstado()
                );
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }
    
    @PutMapping("/BankParameters/edit/{id}")
    public ResponseEntity<BankParameter> editBankById(
    		@PathVariable String id,
    		@RequestBody BankParameter bankParameter
    )
    		throws Exception
    {
        service.getBankById(
        		id,
        		bankParameter.getNombre(),
        		bankParameter.getEntidad(),
        		bankParameter.getEstado()
        		);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }
     
    @DeleteMapping("/BankParameters/delete/{id}")
    public ResponseEntity<Void> deleteBankById(
    		@PathVariable("id") String id
    )
            throws Exception 
    {
        service.deleteBankById(id);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }
}